package com.zuitt.example;

//This will become a child class of Animal class
    //"extends" keyword is used to inherit all the properties and methods of the parent class.
//You can only inherit from 1 class
public class Dog extends Animal{

    //kung mag add ng property
    private String breed;



    //constructor empty
    public Dog(){
        super();
    }

    //constructor parametarized
    public Dog(String name, String color, String breed){
        super(name, color);
        //added property
        this.breed = breed;
    }

    //Getter ng bagong property
    public String getBreed(){
        return this.breed;
    }

    //Setter ng bagong property
    public void setBreed(String breed){
        this.breed = breed;
    }

    //method
    public void makeSound(){
        System.out.println("Woof woof");
    }
}
