package com.zuitt.example;

public class Car {
    //Access Modifiers
    //These are used to restrict the scope of a class, constructor, variable, method or data member.

    //Four Types of Access Modifiers:
        //1. Default - No Keyword indicated (Accessibility is within the package)
        //2. Private (private) - properties or method is only accessible within the class
        //3. Protected - properties and methods are only accessible but the class of the same package and the subclass present in any package
        //4. Public - properties and methods that can be accessed anywhere

    //Class Creation
    //1. Properties - characteristics of an object
    private String name;
    private String brand;
    private int yearOfMake;

    //Make a component for a car through composition
    private Driver driver;


    //2. Constructor - used to create/instantiate an object. The constructor will always be in public
        //a. empty constructor - creates object that doesn't have any arguments/parameters. Also referred as default constructor.
        public Car(){
            this.name = "Empty Car Name";
            this.brand = "Empty Car Brand";
            this.yearOfMake = 0;
        }

        //b. parameterized constructor - creates an object with argument/parameters
        public Car(String name, String brand, int yearOfMake){
            this.name = name;
            this.brand = brand;
            this.yearOfMake = yearOfMake;
            this.driver = new Driver("Manong");
        }

    //3. Getters/Setters - methods that get and set the values of each property of an object. Keep in mid that the number of setter and getters depends on the number of properties
        //Getters - it retrieves the value of instantiate object.

        //Getter for the name property:
        public String getName(){
            return this.name;
        }

        //Getter for the brand property
        public String getBrand(){
            return this.brand;
        }

        //Getter for the yearOfmake property
        public int getYearOfMake(){
            return this.yearOfMake;
        }

        //Getter for the Driver
        public String getDriverName(){
            return this.driver.getName();
        }

        //Setters - use to change the default value of an instantiated object

        //Setter for the name property
        public void setName(String name){
            this.name = name;
        }

        //Setter for the brand property
        public void setBrand(String brand){
            this.brand = brand;
        }

        //Setter for the brand property
        public void setYearOfMake(int yearOfMake){
            this.yearOfMake = yearOfMake;
        }

        //Setter for Driver
        public void setDriverName(String driver){
            this.driver.setName(driver);
        }

    //4. Methods(optional) - function an object can perform (behavior)
        public void drive(){
            System.out.println("The car is running. Vroom Vroom.");
        }

}
