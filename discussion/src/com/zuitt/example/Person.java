package com.zuitt.example;

//pwede magimplement ng multiple interface
public class Person implements Actions, Greetings{
    public void sleep(){
        System.out.println("Zzzzzzz....");
    }

    public void run(){
        System.out.println("Running....");
    }

    public void morningGreet(){
        System.out.println("Good morning");
    }

    public void afternoonGreet(){
        System.out.println("Good afternoon");
    }
}
